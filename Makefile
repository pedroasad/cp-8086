dosbox: tools/dosbox-0.74.tgz
	@tar -xvzf tools/dosbox-0.74.tgz
	@tar -xvzf tools/masm4.tgz -C dos
	@ln -sT dosbox-0.74/src/dosbox dosbox

.PHONY:
addall:
	@git add dos/*.asm

.PHONY:
clean:
	@git clean -d -f -x
