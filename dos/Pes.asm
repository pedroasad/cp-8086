PES     segment         ; verifica se dois numeros sao primos entre si

        assume  cs:PES

DOS       equ 21h
STDOUT    equ  2h
SYS_WRITE equ 40h

Princ   proc
        xor     bx,bx

				; Le dois numeros, verifica se maiores ou 
				; iguais a dois e os armazena em 'si' e 'di'
        call    LeNum
        cmp     di,2
        jb      Erro
        push    di
        call    Lenum
        cmp     di,2
        jb      Erro
        pop     si

        mov     cx,si
        cmp     cx,di
        jbe     Testa
        mov     cx,si
Testa:  xor     dx,dx
        mov     ax,si
        div     cx
        or      dx,dx
        jnz     proxC
        mov     ax,di
        div     cx
        or      dx,dx
        jz      achei
ProxC:  dec     cx
        jmp     Testa
Achei:  push    cs
        pop     ds
        lea     dx,Nao
        mov     ax,TamNao
        dec     cx
        jnz     Escreve
        add     dx,4
        sub     ax,4
Escreve:mov     cx,ax
        inc     bx
        mov     ah,SYS_WRITE
        int     DOS
        mov     ax,4c00h
        int     DOS
Erro:   push    cs
        pop     ds
        lea     dx,msgerr
        mov     cx,TamErr
        mov     bx,STDOUT
        mov     ah,SYS_WRITE
        int     DOS
        mov     ax,4c01h
        int     DOS
Princ   endp

Nao     db      "nao sao primos entre si"
TamNao  equ     $-Nao

MsgErr  db      "Valor invalido!"
TamErr  equ     $-MsgErr

LeNum   proc
        push    ss
        pop     ds
        xor     di,di
        mov     si,10
LeAlg:  mov     ah,3FH
        mov     cx,1
        push    cx
        mov     dx,sp
        int     DOS
        pop     cx
        jc      FimNum
        or      ax,ax
        jz      FimNum
        sub     cl,'0'
        cmp     cx,si
        jae     FimNum
        mov     ax,si
        mul     di
        or      dx,dx
        jnz     FimNum
        add     ax,cx
        jc      FimNum
        mov     di,ax
        jmp     LeAlg
FimNum: ret
LeNum   endp

PES     ends

pilha   segment stack
espaco  db      200 dup ( "PILHA!" )
pilha   ends

        END     Princ
