	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;;; Programa que imprime na sa�da padr�o um texto qualquer. ;;;
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

ESCTELA	SEGMENT
	ASSUME CS:ESCTELA

	INICIO:	
		PUSH	CS				;APONTANDO PARA O SEGMENTO DO PGM
		POP		DS				;PARA MANIPULAR ARQUIVOS, SEMPRE USO O ENDERE�O QUE EST� EM DS:DX
		LEA		DX, TEXTO		;�REA DE MEM�RIA ONDE EST� O TEXTO - O DOS S� MANIPULA DADOS EM MEM.!!!
		MOV		BX, 1			;NAA DA SA�DA PADR�O
		MOV		CX, TAMTEXTO	;TAMANHO DO TEXTO A SER ESCRITO (N�MERO DE BYTES)
		MOV		AH, 40h
		INT 	21h
		CMP		AX, TAMTEXTO	;ax volta com a quantidade de bytes escritos
		JNZ		ERROESCR		;N�O ESCREVEU DIREITO
	SAIDA:
		MOV		AX, 4C00h		;TERMINOU OK
		INT		21h
	ERROESCR:	
		MOV		AX, 4C01h
		JMP		SAIDA
		
		TEXTO		DB	"Oi, mundo!"
		TAMTEXTO	EQU		$-TEXTO

ESCTELA		ENDS				;AQUI � QUE TERMINA O SEGMENTO, PRA COME�AR OUTROS, COMO O DA PILHA!

PILHA	SEGMENT	STACK			
	ESPACO	DB	200	DUP("PILHA!")
PILHA	ENDS

		END	INICIO				;AQUI TERMINAMOS O PROGRAMA
