ContaP  segment
        assume  CS:ContaP
Dos     equ     21H
Princ   proc
        mov     ax,3D00h
        lea     dx,NomeArq
        push    cs
        pop     ds
        int     dos
        jc      ErroAb
        mov     bx,ax
        xor     si,si   ; contador de palavras
        push    ss
        pop     ds
        mov     cx,1
LeMais: push    cx
        mov     dx,sp
        mov     ah,3FH
        int     dos
        pop     dx
        jc      ErroLe
        or      ax,ax
        jz      FimArq
        or      dl,20h
        cmp     dl,'a'
        jb      LeMais  ; SE NAO EH LETRA
        cmp     dl,'z'
        ja      LeMais  ; SE NAO EH LETRA
        inc     si
PrxLet: push    cx
        mov     dx,sp
        mov     ah,3FH
        int     dos
        pop     dx
        jc      ErroLe
        or      ax,ax
        jz      FimArq
        or      dl,20h
        cmp     dl,'a'
        jb      LeMais
        cmp     dl,'z'
        ja      LeMais
        jmp     PrxLet
ErroAB: mov     ax,4C01H
        int     Dos
ErroLe: mov     ax,4C02H
        int     Dos
FimArq: mov     bx,1
        mov     AX,SI
        mov     BP,100
        mov     cl,10
        mov     SI,sp
div100: xor     dx,dx
        div     BP
        xchg    ax,dx
        DIV     cl
        add     ax,"00"
        push    ax
        xchg    ax,dx
        or      ax,ax
        jnz     div100
        cmp     dl,"0"
        mov     dx,sp
        jnz     Escr
        inc     DX
Escr:   mov     cx,SI
        sub     cx,dx
        mov     ah,40h
        int     Dos
        mov     sp,SI
        mov     ax,4C00h
        int     dos
Princ   endp

NomeArq db      "Texto.txt", 0

ContaP  ends

Pilha   segment stack
Espaco  db      200 dup ( "PILHA!" )
Pilha   ends

        END     Princ

